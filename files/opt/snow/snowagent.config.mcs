<?xml version="1.0" encoding="UTF-8"?>
<!-- MCS: This is based on the configuration distributed with the UIS Snow
     Inventory Agent package, version 6.3.0-1.  MCS-specific changes are
     marked with "MCS:". -->
<Configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="snowclient_settings.xsd">
	<Agent>
		<SiteName>UIS</SiteName>
		<ConfigName>UIS</ConfigName>
	</Agent>

  <!-- Server end point settings to send snowpack file after agent scan -->
	<Server>
		<Endpoint>
			<Address>https://snow-inv.uis.cam.ac.uk:443</Address>
			<!-- Proxy settings to use for this endpoint -->
			<Proxy></Proxy>

			<!-- The client certificate used to secure the connection between agent and server -->
			<ClientCertificate>
				<!-- The file name of a client certificate to use (relative the current installation location)--> 
				<FileName></FileName>
				<!-- The password to use if the client certificate has been password protected -->
				<Password></Password>
			</ClientCertificate>
		</Endpoint>
	</Server>

	<!-- Delivery location for scan results -->
	<DropLocation>
		<Path></Path>
	</DropLocation>

	<!-- Configuration for File System scan-->
	<Software>
		<Include>
			<Path recursive="true" unconditionally="false">/*</Path>
		</Include>
		<IncludeCriteria enabled="true">
			<FileType>ELF*executable*</FileType>
		</IncludeCriteria>
		<Exclude>
			<!-- Normal temporary directories on Unix systems. -->
			<Path>/tmp*</Path>
			<Path>/var/tmp*</Path>

			<!-- Oracle audit files can be numerous. -->
			<Path>*/oracle/*/audit*</Path>

			<!-- MCS: Don't attempt to scan DS-Filestore. -->
			<Path>/home/*</Path>
			<Path>/servers/*</Path>
			<!-- MCS: Don't scan old root snapshots. -->
			<Path>/mnt/root/*</Path>

			<!-- Common non-relevant file extensions -->
			<Path>*.au</Path>
			<Path>*.bmp</Path>
			<Path>*.bmu</Path>
			<Path>*.cfg</Path>
			<Path>*.class</Path>
			<Path>*.conf</Path>
			<Path>*.csm</Path>
			<Path>*.css</Path>
			<Path>*.dic</Path>
			<Path>*.enc</Path>
			<Path>*.gif</Path>
			<Path>*.h</Path>
			<Path>*.htm</Path>
			<Path>*.html</Path>
			<Path>*.jpg</Path>
			<Path>*.js</Path>
			<Path>*.log</Path>
			<Path>*.mo</Path>
			<Path>*.mof</Path>
			<Path>*.packlist</Path>
			<Path>*.pcf</Path>
			<Path>*.pc</Path>
			<Path>*.pf</Path>
			<Path>*.pl</Path>
			<Path>*.png</Path>
			<Path>*.po</Path>
			<Path>*.properties</Path>
			<Path>*.rdf</Path>
			<Path>*.sdl</Path>
			<Path>*.so</Path>
			<Path>*.sql</Path>
			<Path>*.ttf</Path>
			<Path>*.txe</Path>
			<Path>*.txt</Path>
			<Path>*.utf8</Path>
			<Path>*.xml</Path>
			<Path>*.xpt</Path>
			<Path>*.zip</Path>
		</Exclude>
	</Software>

	<!-- Scanning for Oracle databases, requires SIOS  -->
	<Oracle enabled="false">
		<!-- If no user credential for specific Instance is given this is used on all other Intances. -->
		<!-- <DefaultInstanceCredentials>
			<UserName></UserName>
			<Password></Password>
		</DefaultInstanceCredentials> -->
		<Include>
			<!-- Either AllInstances or one or more <Instance> -->
			<AllInstances>true</AllInstances>
		</Include>
	</Oracle>

	<Logging>
		<MaxSize>10240</MaxSize>
		<!-- error, warning, info, trace, verbose -->
		<Level>verbose</Level>
	</Logging>

	<SystemSettings>
		<!-- Running processes -->
		<Setting key="software.scan.running_processes" value="true"/>

		<!-- Scan jar files metadata -->
		<Setting key="software.scan.jar" value="true" />  

		<!-- Scan package manager for software inventoring on Debian, SUSE, Ubuntu -->
		<Setting key="software.scan.dpkg" value="true" />

		<!-- Scan package manager for software inventoring on RedHat and CentOS-->
		<Setting key="software.scan.rpm" value="true" />  

		<!-- Check for server certificate -->
		<Setting key="http.ssl_verify" value="false"/>

		<!-- Append the log files -->
		<Setting key="log.append" value="false"/>
		
		<!-- Storage folder for snowpacks -->
		<!-- MCS: Moved from /opt/snow to somewhere writable. -->
		<Setting key="env.data_dir" value="/var/lib/snow/"/>

		<!-- Logging folder for snowagent.log -->
		<!-- MCS: Moved from /opt/snow to somewhere writable. -->
		<Setting key="env.log_dir" value="/var/log/snow/"/>

		<!-- Java executable to run SIOS.jar, configure if java.exe is not in the PATH -->
		<!-- <Setting key="env.java_home" value="java/bin/java"/> -->
	</SystemSettings>
</Configuration>
